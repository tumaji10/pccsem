Návod:

- kompilace pomocí cmake
- ./Determinant, ./Determinant -h / --help, => zobrazí help
- ./Determinant -f / --file <název souboru s maticí> => spustí program a vypočte determinant
- je možné využít data ze složky testData
- v případě potřeby je možné využít druhý pomocný program matrixGenerator, který generuje
	matice ve správném formátu, parametry matice se nastavují v kódu (SIZE, dist(min, MAX))

\
Formát vstupních dat:

n (velikost)\
a11 a12 ... a1n\
a21 a22 ... a2n\
...\
an1 an2 ... ann

3\
1 2 1\
3 4 5\
1 4 5

\
Zadání:

Výpočet determinantu čtvercové matice

Determinant lze jednoduše počítat dle definice nebo Laplaceovou expanzí,
v praxi tyto algoritmy ale nejsou vhodné kvůli své časové složitosti. 
Efektivní implementace jsou založené na Gaussově eliminační mětodě (LU dekompozice).
Očekáváme, že váš program bude schopen pracovat s maticemi 1000×1000 v rozumném čase 
(tj. řádově vteřiny, max. desítky vteřin). Aplikace na vstupu dostane čtvercovou matici, 
na výstupu vypíše hodnotu jejího determinantu.

\
Implementace:

Program obsahuje dvě hlavní funkce, gaussElimination a determinant. 
Jako první se na načtenou matici z textového souboru použije funkce gaussElimination,
která matici promění do horního trojúhelníkového tvaru. Následně druhá funkce vypočte
determinant pomocí pronásobení členů na hlavní diagonále.

\
Komentář:

Při využití LU dekompozice dochází k velkému množství výpočtů a datový typ double naráží na limity.
Pokud bych algoritmus implementoval znovu, využil bych jiného přístupu méně náchylnému
na nepřesnosti výpočtů, např. iterativní metodu.

