#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

bool gaussElimination(double**, int);
double determinant(double**, int);
void printMatrix(double**, int, const string&);
void printUsage();

int main(int argc, char* argv[]) {

    if (argc == 1) {
        // No arguments were provided
        printUsage();
        return 1;
    }

    string filename;

    // Process input
    for (int i = 1; i < argc; ++i) {
        string arg = argv[i];
        if (arg == "-h" || arg == "--help") {
            // Print the usage message and exit
            printUsage();
            return 0;
        } else if (arg == "-f" || arg == "--file") {
            // Check if a filename was provided
            if (++i >= argc) {
                cerr << "Error: No file provided after -f flag" << endl;
                return 1;
            } else filename = argv[i];
        } else {
            cerr << "Error: Unrecognized argument " << arg << endl;
            return 1;
        }
    }

    // Open the file and process it
    ifstream file(filename);
    if (!file.is_open()) {
        cerr << "Error: Could not open file " << filename << endl;
        return 1;
    }
    cout << "Processing file " << filename << endl;
    // Read the size of the matrix
    int size;
    file >> size;

    // Allocate memory for the matrix
    auto** matrix = new double*[size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new double[size];
    }

    // Read the elements of the matrix
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            file >> matrix[i][j];
        }
    }

    file.close();

    // Perform Gaussian elimination and (optional, commented out) print the matrix states before and after

//    printMatrix(matrix, size, "originalMatrix.txt");
    bool changeSign = gaussElimination(matrix, size);
//    printMatrix(matrix, size, "upperTriangleMatrix.txt");

    // Calculate result
    double result = determinant(matrix, size);

    // Add - sign if rows were swapped odd number of times
    if (changeSign) {
        result = result * (-1);
    }

    // Print the determinant
    cout << "The determinant is: " << result << endl;

    // Deallocate memory for the matrix
    for (int i = 0; i < size; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;

    return 0;
}


void printUsage() {
    cout << "Usage: program [-h] [-f <filename>]" << endl;
    cout << "  -h / --help                Print this help message" << endl;
    cout << "  -f / --file <filename>     Process the file with the given name" << endl << endl;
    cout << "File format: " << endl;
    cout << "n (size) " << endl;
    cout << "a11 a12 ... a1n" << endl << "a21 a22 ... a2n" << endl << "..." << endl << "an1 an2 ... ann" << endl;
}

// Function to perform Gaussian elimination
bool gaussElimination(double** matrix, int size) {

    bool changedSign = false;

    for (int i = 0; i < size - 1; i++) {

        // Find the maximum element in column i
        int maxRow = i;
        for (int j = i + 1; j < size; j++) {
            if (fabs(matrix[j][i]) > fabs(matrix[maxRow][i])) {
                maxRow = j;
            }
        }

        // Change sign if swapping rows
        if (maxRow != i){
            changedSign = !changedSign;

            // Swap the maximum row with the current row
            for (int j = 0; j < size; j++) {
                double temp = matrix[i][j];
                matrix[i][j] = matrix[maxRow][j];
                matrix[maxRow][j] = temp;
            }
        }

        // Perform Gaussian elimination
        for (int j = i + 1; j < size; j++) {
            double factor;
            if (matrix[i][i] != 0){
                factor = matrix[j][i] / matrix[i][i];
            } else factor = 0;

            for (int k = i; k < size; k++) {
                matrix[j][k] -= factor * matrix[i][k];
            }
        }
    }
    return changedSign;
}

// Function to calculate the determinant of a square upper triangle matrix
double determinant(double** matrix, int size) {
    // Base case: if the matrix is 1x1, its determinant is simply its only element
    if (size == 1) {
        return matrix[0][0];
    }

    // Initialize the determinant to 1
    double det = 1;

    // Multiply the diagonal elements to find the determinant
    for (int i = 0; i < size; i++) {
        det *= matrix[i][i];

        // -0 solution
        if (det == 0){
            det = 0;
        }
    }

    return det;
}

void printMatrix(double** matrix, int size, const string& name) {
    ofstream out(name);

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            out << matrix[i][j] << " ";
        }
        out << endl;
    }
}

