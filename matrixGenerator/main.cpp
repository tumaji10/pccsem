#include <iostream>
#include <fstream>
#include <random>

using namespace std;

const int SIZE = 1000;

int get_random_double() {
    static mt19937 mt{ std::random_device{}() };
    static uniform_real_distribution<> dist(-100, 200);
    return dist(mt);
}

int main() {

    ofstream out("matrix.txt");

    out << SIZE << endl;

    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            out << get_random_double() << " ";
        }
        out << endl;
    }

    std::cout << "Matrix Generated!" << std::endl;

}
